<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
        include 'menu.php';
    ?>

    <section id="corpo-lojas">

        <div id="em-breve"><h1> Sobre Nós</h1>
        <!-- <img src="_img/logo-marrom.png"></div><br><br><br>  -->
            </div>

            <div class="sobre-nos">
                <table>
                    <tr>
                        <td>
                            <img src="_img/logo_final_2016.png">
                        </td>
                        <td valign="top">
                            <h5>A Criação...</h5>
                            <h2>Criada em 2013, a Chocolateria Brasileira nasceu com o objetivo de atender aos mais exigentes paladares com produtos de alta qualidade e preços justos.<br>
                                No primeiro ano de vida a Chocolateria se especializou em chocolates finos em parceria com grandes Chefs Chocolatier do mercado nacional e internacional para que pudesse levar aos seus clientes uma experiência única em chocolates.<br></h2>

                        </td>

                    </tr>
                    <tr>
                        <td colspan="2">
                            <h5>Primeiros Passos...</h5>
                            <h2>Após os produtos finalizados e testados, a pedidos dos clientes da cidade de origem da Chocolateria Brasileira, Itatiba no interior de São Paulo, inauguramos uma pequena loja num espaço mínimo de 10 metros quadrados com o título de loja de fábrica.
                            <br>
                                O projeto deu certo e em 2016 a fábrica e sua loja foram ampliadas pela terceira vez, dando de presente aos clientes uma loja completa e reformulada com 50 metros quadrados de conforto e muito sabor, onde seus clientes assistem em tempo real parte da produção de seus chocolates especiais e podem viver uma experiência única com nossas receitas exclusivas de chocolates e cafés.<br>
                                <h5>Dias Atuais...</h5>
                                <h2>Constantemente seus idealizadores buscam aprimoramento e diversidade para a marca, hoje, oferecem em suas 5 lojas, entre próprias e franqueadas, um cardápio completo de cafeteria e chocolates para atender aos mais diferentes gostos e transformar a visita de seus clientes em uma experiência mais doce.</h2>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <br>

            </div>

    </section>

    </div>

<?php
include 'rodape.php';
?>

</body>
</html>