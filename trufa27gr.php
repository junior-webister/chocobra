<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="bombom-e-trufa.php">Trufas e Bombons</a> > Trufa 27gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/trufas-27gr.png">
                </td>
                <td>
                    <h1><big>Trufa 27gr :</big></h1>
                    <h2>Nossa exclusiva trufa na caixinha é o produto de maior destaque da marca, sua apresentação demonstra o cuidado que temos com o chocolate. Embalada individualmente, preserva seu formato a transformando em um delicioso presente para o dia a dia, além do sabor elogiado por milhares de clientes.<br><br>Disponíveis nos sabores:<br>
                        - Tradicional<br>
                        - Meio a Meio (Tradicional e Branco)<br>
                        - Puro Cacau<br>
                        - Marula<br>
                        - Cereja<br>
                        - Morango<br>
                        - Meio a Meio (Tradicional e Morango)<br>
                        - Maracujá<br>
                        - Limão<br>
                        - Coco

                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>