<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="bombom-e-trufa.php">Trufas e Bombons</a> > Bombom trufado 15gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/trufa-cereja.png">
                </td>
                <td>
                    <h1><big>Bombom Trufado 15gr :</big></h1>
                    <h2>Bombons de chocolate ao leite com deliciosos recheios trufados, com receitas desenvolvidas inspiradas nos mais famosos Gelatos italianos.<br><br>Disponíveis nos sabores:<br>
                        - Branco<br>
                        - Frutas vermelhas<br>
                        - Pistache<br>
                        - Menta<br>
                        - Café<br>
                        - Laranja<br>

                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>