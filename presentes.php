<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>


    <section id="corpo-lojas">
        <h1>Presentes</h1>

        <ul class="albun-fotos2">
            <a href="leiteira-bombons-150-300gr.php"><li id="foto13"><span>Leiteira com Bombons 150 gr e 300gr</span></li></a>
            <a href="cachepot.php"><li id="foto14"><span>Cachepot com Bombons 150gr</span></li></a>
            <a href="caneca-bombons.php"><li id="foto15"><span><small>Caneca de Porcelana com Bombons 180gr</small></span></li></a>
        </ul>
        <ul class="albun-fotos2">
            <a href="caixa-selecoes.php"><li id="foto16"><span>Caixa Seleções 160gr</span></li></a>
            <a href="lata-celebrar-225gr.php"><li id="foto17"><span>Lata Celebrar 225gr</span></li></a>
            <a href="mix-barrinhas.php"><li id="foto18"><span>Mix de Barrinhas 200gr</span></li></a>
        </ul>

        <!--
        <table border="0" width="80%" id="produtos">
            <tr>
                <td align="center">
                    <a href="leiteira-bombons-150-300gr.php"><img src="_img/leiteira.png"></a>
                </td>
                <td>
                    <a href="cachepot.php"><img src="_img/cachepot.png"></a>
                </td>
                <td>
                    <a href="caneca-bombons.php"><img src="_img/caneca.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Leiteira com Bombons 150 gr e 300 gr</h4>
                </td>
                <td>
                    <h4>Cachepot com Bombons 150 gr</h4>
                </td>
                <td>
                    <h4>Caneca de Porcelana com Bombons 180 gr</h4>
                </td>
            </tr>
            <tr>
                <td align="center"><br><br><br><br><br><br><br>
                    <a href="caixa-selecoes.php"><img src="_img/caixa-selecoes.png"></a>
                </td>
                <td valign="bottom">
                    <a href="lata-celebrar-225gr.php"><img src="_img/leiteira.png"></a>
                </td>
                <td valign="bottom">
                    <a href="mix-barrinhas.php"><img src="_img/mix-barrinhas.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Caixa Seleções 160 gr</h4>
                </td>
                <td>
                    <h4>Lata Celebrar 225gr</h4>
                </td>
                <td>
                    <h4>Mix de Barrinhas 200 gr</h4>
                </td>
            </tr>
            <tr>
                <td align="center"><br><br><br><br><br><br><br>
                    <a href="brigadeiro-gourmet.php"><img src="_img/brigadeiro-gourmet.png"></a>
                </td>
                <td valign="bottom">
                    <a href="cookies.php"><img src="_img/cookies.png"></a>
                </td>

            </tr>
            <tr>
                <td>
                    <h4>Brigadeiro Gourmet 240 gr </h4>
                </td>
                <td>
                    <h4>Cookies Integrais 60 gr</h4>
                </td>

            </tr>
        </table>
        -->
    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>