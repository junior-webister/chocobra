<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" href="_css/form.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
        include 'menu.php';
    ?>

    <section id="corpo-franqueado">

            <h1>Seja Franqueado</h1>
        <table class="conteudo-franqueado" border="0">
            <tr>
                <td colspan="3" id="teste-tr">
                    <br>
                    <h3><big><big>- INVESTIMENTOS (valores estimados): </big></big></h3>
                    <br>
                </td>
            </tr>
            <tr>
                <td width="49.9%">
                    <h4 id="texto-franqueado-esq"><small>Loja Padrão</small></h4><br>
                </td>
                <td rowspan="2" id="miolo">

                </td>
                <td  width="49.9%">
                    <h4 id="texto-franqueado-esq"><small>Quiosque</small></h4><br>
                </td>
            </tr>
            <tr>
                <td>
                    <h2 id="texto-franqueado-esq">Taxa de Franquia: ..................................<b>R$ 40.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Reforma, Instalações, Equipamentos: ...<b>R$ 130.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Pedido Inicial: ........................................<b>R$ 30.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Capital de Giro: ......................................<b>R$ 50.000,00</b>
                    <br>
                        <small>(Custo total não compreende investimento de CDU/Luvas)</small></h2>
                    <h2 id="texto-franqueado-esq">Total do Investimento: ...........................<b>R$ 250.000,00</b></h2>
                </td>
                <td>
                    <h2 id="texto-franqueado-esq">Taxa de Franquia: .......................<b>R$ 30.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Instalações, Equipamentos: .......<b>R$ 70.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Pedido Inicial: .............................<b>R$ 20.000,00</b></h2>
                    <h2 id="texto-franqueado-esq">Capital de Giro: ...........................<b>R$ 30.000,00</b></h2>
                    <br>
                    <h2 id="texto-franqueado-esq">Total do Investimento: ...............<b>R$ 150.000,00</b></h2>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <br>
                    
                    <br>
                </td>
            </tr>
            <tr>
                <td colspan="3" id="teste-tr">
                    <br>
                    <h3><big><big>- EM NÚMEROS:</big></big></h3>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <h2 id="texto-franqueado">Prazo Estimado de Retorno do Investimento: </h2>
                    <h2 id="texto-franqueado">Margem de Lucro Liquida Estimada: </h2>
                    <h2 id="texto-franqueado">Taxa de Royalties: </h2>
                    <h2 id="texto-franqueado">Fundo de Promoção: </h2>
                    <h2 id="texto-franqueado">Taxa de Publicidade: </h2>
                </td>
                <td>
                    <h2 id="texto-franqueado-esq"><b>24 a 36 meses</b></h2>
                    <h2 id="texto-franqueado-esq"><b>18 a 23%</b></h2>
                    <h2 id="texto-franqueado-esq"><b>Mensalidade fixa + 7% sobre vendas de cafeteria</b></h2>
                    <h2 id="texto-franqueado-esq"><b>Mensalidade Fixa</b></h2>
                    <h2 id="texto-franqueado-esq"><b>Isento</b></h2>
                </td>
            </tr>
            <br>
            <br>
            <br>
            <br>
            <tr>
                <td colspan="3">
                    <br><br>
                </td>
            </tr>

            <tr>
                <td colspan="3" id="teste-tr">
                    <br>
                    <br>
                    <h3><big><big>- APOIO AO FRANQUEADO:</big></big></h3>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <li><h2>Apoio na escolha e negociação do ponto comercial</h2></li>
                    <li><h2>Suporte em todas as etapas do cronograma</h2></li>
                    <li><h2>Treinamento e capacitação</h2></li>
                    <li><h2>Apoio no marketing e divulgação</h2></li>
                    <li><h2>Software de gestão integrado</h2></li>
                    <li><h2>Preferência na expansão</h2></li>
                </td>
            </tr>
        </table>

        <br>
        <h4><small><a href="contato-franqueado.php"><span class="cor">Clique aqui</span></a> para receber maiores informações</small></h4>
        <br>
        <br>
    </section>

</div><br><br>

<?php
    include 'rodape.php';
?>

</body>
</html>