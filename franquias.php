<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>

<?php
    include 'menu.php';
?>

<section id="corpo-lojas">

    <h1>Nossas Lojas</h1>
    <h4><small><small>Quer ser um franqueado? <a href="contato.php">Entre em contato</a> conosco para mais informações</small></small></h4>
    <br><br>
    <?php
    include 'slider2.php';
    ?>

    <div class="catalogo">
        <h1><small>Encontre-nos através dos endereços:</small></h1>
    </div>
    <table class="enderecos" cellspacing="25px">
        <tr>
            <td>
                <h4>Itatiba/SP – Loja de Fábrica </h4>
                <h2>Av. Campinas, 70 - Vila Brasileira<br>
                    F. 11-4534-3637<br>
                </h2>
            </td>
            <td>
                <h4>Sorocaba/SP – Shopping Pátio Ciane</h4>
                <h2>Quiosque 3 – Piso térreo <br>
                </h2>
            </td>
            <td>
                <h4>Brasília/DF <br> Shopping Venâncio 2000</h4>
                <h2>Piso térreo – Nova praça de Alimentação <br>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h4>Brasília/DF – Sudoeste </h4>
                <h2>CLSW Quadra 303 – Bloco C – Loja 12 <br>
                    Setor Sudoeste <br>
                </h2>
            </td>
            <td>
                <h4>Brasília/DF – ParkShopping</h4>
                <h2>Piso térreo loja 117 A – ao lado da Riachuelo <br>
                </h2>
            </td>
            <td>
                <h4><small>Santos/SP - Distribuidor Exclusivo</small> </h4>
                <h2>Telefone: (13) 98154-4954<br>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <h4><small>Águas Claras/DF - Residencial Villa Matheus </small></h4>
                <h2>Rua 35 Sul - lojas 3 e 4
                    <br>
                </h2>
            </td>
            <td>
                <h4>Goiânia/GO</h4>
                <h2>Shopping Passeio das Águas
                    <br>
                </h2>
            </td>
            <td>
                <h4><small>São Paulo/SP – Morumbi - Em Breve</small></h4>
                <h2>Rua Dr. Luis Migliano, 977<br></h2>
            </td>   
        </tr>
        <tr>
            <td>
                <h4><small>Campinas/SP – Cambuí - Em Breve</small></h4>
                <h2>Rua Coronel Silva Teles, 165<br></h2>
            </td>
            <td>
                <h4><small>Jundiaí/SP – Vila Virginia - Em Breve</small></h4>
                <h2>Av. Nove de Julho, 1500<br></h2>
            </td>
            <td>
                <h4><small>São Paulo/SP <br> Shopping Cosmopolitano – Em Breve</small></h4>
                <h2><br></h2>
            </td>
        </tr>
        <tr>
            <td>
                <h4>João Pessoa/PB - Em Breve</h4>
                <h2><br></h2>
            </td>
            <td>
                <h4>Valparaiso/GO – Shopping Sul Valparaiso - Ala Sul </h4>
                <h2><br>
                </h2>
            </td>
            <td>
                <h4>Uberlândia/MG – Em Breve </h4>
                <h2><br>
                </h2>
            </td>
            
        </tr>
        <tr>
            <td>
                <h4>Moema/SP – Em Breve </h4>
                <h2><br>
                </h2>
            </td>
            <td>
                <h4>Alfa Shopping (Alphaville) - Em Breve</h4>
                <h2><br></h2>
            </td>
        </tr>


        <!--
        <tr>
            <td colspan="3">
                <h4><small>Em Breve na Região Sudoeste</small></h4>
            </td>
        </tr>
        -->
    </table>


</section>

</div>
<br><br>

<?php
include 'rodape.php';
?>

</body>
</html>