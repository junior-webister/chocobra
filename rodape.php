<div class="enter-up-bounce">
    <footer class="rodape">
        <!-- <li> -->
        <table border="0" align="center" width="auto" valign="top" class="tabela-rodape">
            <tr>
                <td colspan="11">Chocolateria Brasileira © 2016</td>
            </tr>
            <td valign="middle">
                <small><a href="#"><h2>Todos os direitos reservados</h2></a></small>
            </td>
            <td class="barrinha">
                |
            </td>
            <td valign="middle">
                <small><a href="#"><h2>Política de privacidade</h2></a></small>
            </td>
            <td class="barrinha">
                |
            </td>
            <td valign="middle">
                <small><a href="#"><h2>Vendas corporativas</h2></a></small>
            </td>
            <td class="barrinha">
                |
            </td>
            <td valign="middle">
                <small><a href="#"><h2>Mapa do site</h2></a></small>
            </td>
            <td class="barrinha">
                |
            </td>
            <td>
                <small><a href="http://webister.com.br" target="_blank"><h2>Design - Webister©</h2></a></small>
            </td>
            <td class="barrinha">
                |
            </td>
            <td>
                <small><a href="portal/"><h2>Portal do Franqueado</h2></a></small>
            </td>
        </table>
        <!-- </li> -->
        <!-- <li> -->
        <table border="0" align="center" class="tabela-redes">
            <tr>
                <td>
                    <a href="https://www.facebook.com/chocolateriabrasileira/timeline" target="_blank"><img src="_img/social_facebook_box_blue-peq.png"></a>
                </td>
                <td>
                    <a href="https://www.instagram.com/chocolateriabrasileiracps/" target="_blank"><img src="_img/Instagram-logo-peq.png"></a>
                </td>
                <td>
                    <a href="https://twitter.com/hashtag/chocolateriabrasileira?src=hash" target="_blank"><img src="_img/Twitter256-peq.png"></a>
                </td>
            </tr>
        </table>
        <!-- </li> -->
    </footer>
</div>