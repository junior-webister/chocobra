<?php
header('Content-Type: text/html; charset=utf-8');
include "conectar_banco.php";
include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
protegePagina();

$sql = mysql_query("SELECT permissao FROM usuarios where id = " . $_SESSION['usuarioID']);
$resultado = mysql_fetch_assoc($sql);

if ($resultado['permissao'] == 0) {
	$cabecalho = "header-noPermission.php";
} elseif ($resultado['permissao'] == 2) {
	$cabecalho = "header-master.php";
} else {
	$cabecalho = "header.html";
}

require $cabecalho;

	protegePagina(); 
?>

<script type="text/javascript">
	$(function(){
    $(".target-active").find("[href='analises.php']").parent().addClass("active");
});
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Análises - Logs do sistema</h1>
	<div class="table-responsive">
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Hora</th>
			        <th>Endereço IP</th>
			        <th>Mensagem</th>
				</tr>
			</thead>
			<?php
				$logs = mysql_query("select * from logs order by 1 desc");
				while ($ver = mysql_fetch_array($logs)) {
					echo "<tr>";
					echo "<td>".$ver['id']."</td>";
					echo "<td>".$ver['hora']."</td>";
					echo "<td>".$ver['ip']."</td>";
					echo "<td>".$ver['mensagem']."</td>";
					echo "</tr>";
				}
			?>
		</table>
	</div>
</div>

<?php
	require "footer.php";
?>