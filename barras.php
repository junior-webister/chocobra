<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
        include 'menu.php';
    ?>

    <!-- The JavaScript -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="jquery.easing.1.3.js"></script>
    <script type="text/javascript">
        $(function() {
            /**
             * for each menu element, on mouseenter,
             * we enlarge the image, and show both sdt_active span and
             * sdt_wrap span. If the element has a sub menu (sdt_box),
             * then we slide it - if the element is the last one in the menu
             * we slide it to the left, otherwise to the right
             */
            $('#sdt_menu > li').bind('mouseenter',function(){
                var $elem = $(this);
                $elem.find('img')
                    .stop(true)
                    .animate({
                        'width':'170px',
                        'height':'170px',
                        'left':'0px'
                    },400,'easeOutBack')
                    .andSelf()
                    .find('.sdt_wrap')
                    .stop(true)
                    .animate({'top':'140px'},500,'easeOutBack')
                    .andSelf()
                    .find('.sdt_active')
                    .stop(true)
                    .animate({'height':'170px'},300,function(){
                        var $sub_menu = $elem.find('.sdt_box');
                        if($sub_menu.length){
                            var left = '170px';
                            if($elem.parent().children().length == $elem.index()+1)
                                left = '-170px';
                            $sub_menu.show().animate({'left':left},200);
                        }
                    });
            }).bind('mouseleave',function(){
                var $elem = $(this);
                var $sub_menu = $elem.find('.sdt_box');
                if($sub_menu.length)
                    $sub_menu.hide().css('left','0px');

                $elem.find('.sdt_active')
                    .stop(true)
                    .animate({'height':'0px'},300)
                    .andSelf().find('img')
                    .stop(true)
                    .animate({
                        'width':'0px',
                        'height':'0px',
                        'left':'85px'},400)
                    .andSelf()
                    .find('.sdt_wrap')
                    .stop(true)
                    .animate({'top':'25px'},500);
            });
        });
    </script>

    <section id="corpo-lojas">
        <h1>Barras e tabletes</h1>

        <ul class="albun-fotos2">
            <a href="barra-de-chocolate-100gr.php"><li id="foto01"><span>Barra de Chocolate 100gr</span></li></a>
            <a href="barra-de-chocolate-20gr.php"><li id="foto02"><span>Barra de Chocolate 20gr</span></li></a>
            <a href="barra-origem-brasileiro-40gr.php"><li id="foto03"><span><small>Barra Origem 100% Brasileiro 40gr</small></span></li></a>
        </ul>
        <ul class="albun-fotos2">
            <a href="barra-nuts-100gr.php"><li id="foto04"><span>Barra Nuts 100gr</span></li></a>
            <a href="tablete-chocolate-10gr.php"><li id="foto05"><span>Tablete de Chocolate 10gr</span></li></a>
            <a href="placa-frases-50gr.php"><li id="foto06"><span>Placa Frases 50gr</span></li></a>
        </ul>
        <br><br>

        <!--
        <table border="0" width="80%" id="produtos" class="table-produtos">
            <tr>
                <td align="center">
                    <a href="barra-de-chocolate-100gr.php"><img src="_img/barras1.png"></a>
                </td>
                <td>
                    <a href="barra-de-chocolate-20gr.php"><img src="_img/barras20g.png"></a>
                </td>
                <td>
                    <a href="barra-origem-brasileiro-40gr.php"><img src="_img/barras40g.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Barra de Chocolate 100 gr</h4>
                </td>
                <td>
                    <h4>Barra de Chocolate 20 gr</h4>
                </td>
                <td>
                    <h4>Barra de Chocolate Origem 100% Brasileiro 40 gr</h4>
                </td>
            </tr>
            <tr>
                <td align="center"><br><br><br><br><br><br><br><br><br><br>
                    <a href="barra-nuts-100gr.php"><img src="_img/barras-nuts.png"></a>
                </td>
                <td valign="bottom">
                    <a href="tablete-chocolate-10gr.php"><img src="_img/tablete10g.png"></a>
                </td>
                <td valign="bottom">
                    <a href="placa-frases-50gr.php"><img src="_img/barras-nomes.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Barra Nuts 100 gr</h4>
                </td>
                <td>
                    <h4>Tablete de Chocolate 10 gr</h4>
                </td>
                <td>
                    <h4>Placa Frases 50 gr</h4>
                </td>
            </tr>
        </table>
        -->
    </section>

</div><br><br>
<?php
    include 'rodape.php';
?>
</body>
</html>