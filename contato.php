<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" href="_css/form.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
    include 'menu.php';
    ?>

    <section id="corpo-franqueado">

        <div id="textos">
            <h1>Contato</h1>
            <h4><small>Deixe aqui a sua mensagem que retornaremos <br>o mais breve possível!<br>
            Se preferir, entre em contato através dos telefones:</small><br> <small>(11) 4524-0146 / (11) 4524-6516</small> </h4>
            <!--<br><br>
            <a href="seja-franqueado.php" class="btn-franqueado">Seja Franqueado</a> -->
        </div>

        <?php
            include 'form.html';
        ?>

    </section>

</div><br><br>

<?php
include 'rodape.php';
?>

</body>
</html>