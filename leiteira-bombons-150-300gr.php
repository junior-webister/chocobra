<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="presentes.php">Presentes</a> > Leiteira com Bombons 150gr e 300gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/leiteira.png">
                </td>
                <td>
                    <h1><big>Leiteira com Bombons 150gr e 300gr :</big></h1>
                    <h2>Delicado presente em formato de leiteira, produzido em alumínio com estampas variadas, é o presente que agrada e eterniza, tornando-se um lindo objeto de decoração.<br>
                        Recheados com bombons nas duas versões.
                        <br><br>Disponíveis nas versões:<br>
                        - Pequena (150 gr de bombons ao leite)<br>
                        - Grande (300 gr de bombons sortidos)<br>

                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>