<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
    include 'menu.php';
    ?>

    <!-- The JavaScript -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="jquery.easing.1.3.js"></script>
    <script type="text/javascript">
        $(function() {
            /**
             * for each menu element, on mouseenter,
             * we enlarge the image, and show both sdt_active span and
             * sdt_wrap span. If the element has a sub menu (sdt_box),
             * then we slide it - if the element is the last one in the menu
             * we slide it to the left, otherwise to the right
             */
            $('#sdt_menu > li').bind('mouseenter',function(){
                var $elem = $(this);
                $elem.find('img')
                        .stop(true)
                        .animate({
                            'width':'170px',
                            'height':'170px',
                            'left':'0px'
                        },400,'easeOutBack')
                        .andSelf()
                        .find('.sdt_wrap')
                        .stop(true)
                        .animate({'top':'140px'},500,'easeOutBack')
                        .andSelf()
                        .find('.sdt_active')
                        .stop(true)
                        .animate({'height':'170px'},300,function(){
                            var $sub_menu = $elem.find('.sdt_box');
                            if($sub_menu.length){
                                var left = '170px';
                                if($elem.parent().children().length == $elem.index()+1)
                                    left = '-170px';
                                $sub_menu.show().animate({'left':left},200);
                            }
                        });
            }).bind('mouseleave',function(){
                var $elem = $(this);
                var $sub_menu = $elem.find('.sdt_box');
                if($sub_menu.length)
                    $sub_menu.hide().css('left','0px');

                $elem.find('.sdt_active')
                        .stop(true)
                        .animate({'height':'0px'},300)
                        .andSelf().find('img')
                        .stop(true)
                        .animate({
                            'width':'0px',
                            'height':'0px',
                            'left':'85px'},400)
                        .andSelf()
                        .find('.sdt_wrap')
                        .stop(true)
                        .animate({'top':'25px'},500);
            });
        });
    </script>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="barras.php">Barras</a> > Placa Frases 50gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/barras-nomes.png">
                </td>
                <td>
                    <h1><big>Placa Frases 50gr :</big></h1>
                    <h2>Produzidas artesanalmente com chocolate ao leite e branco, um jeito delicioso de expressar seus sentimentos. <br><br>
                        Disponíveis nas versões:<br>
                        - "Com carinho"<br>
                        - "Te amo"<br>
                        - "Parabéns"<br>

                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>

<?php
include 'rodape.php';
?>

</body>
</html>